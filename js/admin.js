//Funciones de Pedidos

function Pedidos() {

	this.insertar = function() {
		$.ajax({
			url: base_url + 'acciones/pedidos/insertar',
			type: 'POST',
			dataType: 'JSON',
			data: $("#form-hacer-pedido").serialize(),
			beforeSend: function() {
				swal({
					type: 'info',
					title: 'Atención',
					text: 'Se están validando los datos...',
					allowEscapeKey: false,
					allowOutsideClick: false
				});
			},
			success: function(response) {
				swal({
					type: response.tipo,
					title: response.titulo,
					html: response.mensaje,
				}).then(function() {
					$("#form-hacer-pedido")[0].reset();
				});
			},
			error: function(err) {
				swal({
					type: 'error',
					title: 'Error',
					text: 'Ha ocurrido un error inesperado, por favor intente más tarde.'
				});
			}
		});
	};

	this.actualizar = function() {
		$.ajax({
			url: base_url + 'acciones/pedidos/actualizar',
			type: 'POST',
			dataType: 'JSON',
			data: $("#form-actualizar-pedido").serialize(),
			beforeSend: function() {
				swal({
					type: 'info',
					title: 'Atención',
					text: 'Se están validando los datos...',
					allowEscapeKey: false,
					allowOutsideClick: false
				});
			},
			success: function(response) {
				swal({
					type: response.tipo,
					title: response.titulo,
					html: response.mensaje,
				}).then(function() {
					//$("#form-hacer-pedido")[0].reset();
				});
			},
			error: function(err) {
				swal({
					type: 'error',
					title: 'Error',
					text: 'Ha ocurrido un error inesperado, por favor intente más tarde.'
				});
			}
		});
	};

	this.eliminar = function(id) {
		swal({
			title: 'Esta seguro?',
			text: "Se va a eliminar un pedido!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si, estoy seguro!'
		}).then(function() {
			$.ajax({
				url: base_url + 'acciones/pedidos/eliminar',
				type: 'POST',
				dataType: 'JSON',
				data: {idpedido: id},
				beforeSend: function() {
					swal({
						type: 'info',
						title: 'Atención',
						text: 'Se están validando los datos...',
						allowEscapeKey: false,
						allowOutsideClick: false
					});
				},
				success: function(response) {
					swal({
						type: response.tipo,
						title: response.titulo,
						html: response.mensaje,
					}).then(function() {
						location.reload();
					});
				},
				error: function(err) {
					swal({
						type: 'error',
						title: 'Error',
						text: 'Ha ocurrido un error inesperado, por favor intente más tarde.'
					});
				}
			});
		})
	}

}

var accionesPedidos = new Pedidos();

//Data tables
$(".data-table").dataTable({
	"language": {
		"sProcessing": "Procesando...",
		"sLengthMenu": "Mostrar _MENU_ registros",
		"sZeroRecords": "No se encontraron resultados",
		"sEmptyTable": "Ningún dato disponible en esta tabla",
		"sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
		"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix": "",
		"sSearch": "Buscar:",
		"sUrl": "",
		"sInfoThousands": ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
			"sFirst": "Primero",
			"sLast": "Último",
			"sNext": "Siguiente",
			"sPrevious": "Anterior"
		},
		"oAria": {
			"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
	}
});


//Formulario para guardar un pedido
$("#form-hacer-pedido").on("submit", function() {
	accionesPedidos.insertar();
	return false;
});

//Formulario para actualizar un pedido
$("#form-actualizar-pedido").on("submit", function() {
	accionesPedidos.actualizar();
	return false;
});

//Boton para eliminar un pedido
$(".btn-eliminar").on("click", function() {
	var idpedido = $(this).data("idpedido");
	accionesPedidos.eliminar(idpedido);
	return false;
});

//Grafica por dia
if(pagina == 'grafica'){
	Morris.Bar({
    element: 'grafica-dia',
    data: [
      {dias: '2017-09-12', cantpedidos: 136},
      {dias: '2017-09-13', cantpedidos: 137},
      {dias: '2017-09-14', cantpedidos: 275},
      {dias: '2017-09-15', cantpedidos: 380},
      {dias: '2017-09-16', cantpedidos: 655},
      {dias: '2017-09-17', cantpedidos: 1571}
    ],
    xkey: 'dias',
    ykeys: ['cantpedidos'],
    labels: ['No. Pedidos'],
    barRatio: 0.4,
    xLabelAngle: 35,
    hideHover: 'auto'
  });
}
