# README #

Presentación de la prueba de para backend

### Tecnologias empleadas? ###

* Codeigniter 3.5
* Admin LTE 2.3.0

### Instrucciones ###

* Descargar el repositorio
* Cambiar la valriable base_url en el archivo application/config.php
* Importar la base de datos que se encuentra en la carpeta db
* Cambiar los datos de conexion a la base de datos en el archivo application/database.php
* Navegar el sitio
