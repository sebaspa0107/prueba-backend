<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
		$this->load->database();
  }

	public function getAll(){

		$this->db->select('id, nombre, precio');
		$query = $this->db->get('productos');

		if($query->num_rows() > 0){
			$respuesta = array(
				'err' => FALSE,
				'data' => $query->result_array()
			);
		}
		else {
			$respuesta = array(
				'err' => TRUE,
				'data' => NULL
			);
		}
		return $respuesta;

	}

}
