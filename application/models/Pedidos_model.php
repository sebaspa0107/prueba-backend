<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
		$this->load->database();
  }

	public function getAll(){
		$this->db->select('id, nomcliente, apecliente, nomproducto, cantidad, fecha, dia');
		$query = $this->db->get('v_pedidos');

		if($query->num_rows() > 0){
			$respuesta = array(
				'err' => FALSE,
				'data' => $query->result_array()
			);
		}
		else {
			$respuesta = array(
				'err' => TRUE,
				'data' => NULL
			);
		}
		return $respuesta;
	}

	public function get($idpedido = FALSE){
		$this->db->select('id, nomcliente, apecliente, idcliente, idproducto, nomproducto, cantidad');
		$this->db->where('id', $idpedido);
		$query = $this->db->get('v_pedidos');

		if($query->num_rows() > 0){
			$respuesta = array(
				'err' => FALSE,
				'data' => $query->result_array()
			);
		}
		else {
			$respuesta = array(
				'err' => TRUE,
				'data' => NULL
			);
		}
		return $respuesta;
	}

	public function insert($datos){
		$query = $this->db->insert('pedidos', $datos);
		if($query)
		{
			$respuesta = array(
				'err' => FALSE,
				'registro_id' => $this->db->insert_id()
			);
		}
		else {
			$respuesta = array(
				'err' => TRUE,
				'error' => $this->db->error()['message'],
				'error_num' => $this->db->error()['code']
			);
		}
	}

	public function update($datos, $idpedido){
		$this->db->where('id', $idpedido);
		$query = $this->db->update('pedidos', $datos);
		if($query)
		{
			$respuesta = array(
				'err' => FALSE
			);
		}
		else {
			$respuesta = array(
				'err' => TRUE,
				'error' => $this->db->error()['message'],
				'error_num' => $this->db->error()['code']
			);
		}
	}

	public function delete($idpedido){
		$this->db->where('id', $idpedido);
		$query = $this->db->delete('pedidos');
		if($query)
		{
			$respuesta = array(
				'err' => FALSE
			);
		}
		else {
			$respuesta = array(
				'err' => TRUE,
				'error' => $this->db->error()['message'],
				'error_num' => $this->db->error()['code']
			);
		}
	}

}
