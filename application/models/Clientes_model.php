<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
		$this->load->database();
    //Codeigniter : Write Less Do More
  }

	public function getAll(){

		$this->db->select('id, nombre, apellido, email');
		$query = $this->db->get('clientes');

		if($query->num_rows() > 0){
			$respuesta = array(
				'err' => FALSE,
				'data' => $query->result_array()
			);
		}
		else {
			$respuesta = array(
				'err' => TRUE,
				'data' => NULL
			);
		}
		return $respuesta;

	}

}
