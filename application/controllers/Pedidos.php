<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

		//Carga de los modelos a usar
		$this->load->model(array('productos_model', 'clientes_model', 'pedidos_model'));
  }

  function index(){
    //Datos de la pagina interna
		$data = array(
			'base_url' => base_url(),
			'estilo' => 'admin',
			'script' => 'admin',
			'pagina' => 'grafica',
			'class_body' => 'hold-transition skin-blue sidebar-mini',
			'title' => 'Home',
			'keywords' => '',
			'descripcion' => ''
		);

		//Plantilla
		$this->parser->parse('includes/header', $data);
		$this->parser->parse('admin/includes/header', $data);
		$this->parser->parse('admin/includes/menu', $data);
		$this->parser->parse('pedidos/grafica', $data);
		$this->parser->parse('admin/includes/footer', $data);
		$this->parser->parse('includes/footer', $data);
  }

  function ingresar()
  {

		//Datos de la pagina interna
		$data = array(
			'base_url' => base_url(),
			'estilo' => 'admin',
			'script' => 'admin',
			'pagina' => 'ingresapedido',
			'class_body' => 'hold-transition skin-blue sidebar-mini',
			'title' => 'Home',
			'keywords' => '',
			'descripcion' => '',
			'productos' => $this->productos_model->getAll()['data'],
			'clientes' => $this->clientes_model->getAll()['data']
		);

		//Plantilla
		$this->parser->parse('includes/header', $data);
		$this->parser->parse('admin/includes/header', $data);
		$this->parser->parse('admin/includes/menu', $data);
		$this->parser->parse('pedidos/ingresar', $data);
		$this->parser->parse('admin/includes/footer', $data);
		$this->parser->parse('includes/footer', $data);
  }

	function vertodos()
  {

		//Datos de la pagina interna
		$data = array(
			'base_url' => base_url(),
			'estilo' => 'admin',
			'script' => 'admin',
			'pagina' => 'vertodos',
			'class_body' => 'hold-transition skin-blue sidebar-mini',
			'title' => 'Home',
			'keywords' => '',
			'descripcion' => '',
			'pedidos' => $this->pedidos_model->getAll()['data']
		);

		//Plantilla
		$this->parser->parse('includes/header', $data);
		$this->parser->parse('admin/includes/header', $data);
		$this->parser->parse('admin/includes/menu', $data);
		$this->parser->parse('pedidos/vertodos', $data);
		$this->parser->parse('admin/includes/footer', $data);
		$this->parser->parse('includes/footer', $data);
  }

	function editar($idpedido = FALSE)
  {
		if(!is_numeric($idpedido))
			show_404();


		//Datos de la pagina interna
		$data = array(
			'base_url' => base_url(),
			'estilo' => 'admin',
			'script' => 'admin',
			'pagina' => 'editar',
			'class_body' => 'hold-transition skin-blue sidebar-mini',
			'title' => 'Home',
			'keywords' => '',
			'descripcion' => '',
			'productos' => $this->productos_model->getAll()['data'],
			'clientes' => $this->clientes_model->getAll()['data'],
			'pedido' => $this->pedidos_model->get($idpedido)['data'],
			'cantidad' => $this->pedidos_model->get($idpedido)['data'][0]['cantidad']
		);

		//Plantilla
		$this->parser->parse('includes/header', $data);
		$this->parser->parse('admin/includes/header', $data);
		$this->parser->parse('admin/includes/menu', $data);
		$this->parser->parse('pedidos/ver', $data);
		$this->parser->parse('admin/includes/footer', $data);
		$this->parser->parse('includes/footer', $data);
  }

}
