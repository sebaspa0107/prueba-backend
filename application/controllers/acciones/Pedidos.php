<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
		$this->load->model(array('pedidos_model'));
		$this->load->library('form_validation');
  }

  function insertar()
  {
		if (!$this->input->is_ajax_request())
			exit('No direct script access allowed');

			$configDataPost = array(

				array(
					'field' => 'cliente',
					'label' => 'cliente',
					'rules' => 'trim|required|is_natural_no_zero'
				),
				array(
					'field' => 'producto',
					'label' => 'producto',
					'rules' => 'trim|required|is_natural_no_zero'
				),
				array(
					'field' => 'cantidad',
					'label' => 'cantidad',
					'rules' => 'trim|required|is_natural_no_zero'
				),
			);

			//cargue de las reglas de validación
			$this->form_validation->set_rules($configDataPost);

			if ($this->form_validation->run() == FALSE)
			{
				$respuesta = array(
					'err' => TRUE,
					'tipo' => 'warning',
					'titulo' => 'Atención!',
					'mensaje' => str_replace("\n", "", validation_errors())
				);

			}
			else
			{

				$datos['idcliente'] = strtolower($this->input->post('cliente'));
				$datos['idproducto'] = strtolower($this->input->post('producto'));
				$datos['cantidad'] = strtolower($this->input->post('cantidad'));
				$datos['fecha'] = date('Y-m-d H:i:s');
				$datos['dia'] = date('Y-m-d');

				$insertar = $this->pedidos_model->insert($datos);

				if($insertar['err'])
				{
					$respuesta = array(
						'err' => TRUE,
						'tipo' => 'warning',
						'titulo' => 'Ups!',
						'mensaje' => $insertar['mensaje']
					);
				}

				else
				{
					$respuesta = array(
						'err' => FALSE,
						'tipo' => 'success',
						'titulo' => 'Super!',
						'mensaje' => 'Su registro se ha hecho correctamente.'
					);
				}
			}
			echo json_encode($respuesta);
			return;
  }

	function actualizar()
  {
		if (!$this->input->is_ajax_request())
			exit('No direct script access allowed');

			$configDataPost = array(

				array(
					'field' => 'idpedido',
					'label' => 'idpedido',
					'rules' => 'trim|required|is_natural_no_zero'
				),
				array(
					'field' => 'cliente',
					'label' => 'cliente',
					'rules' => 'trim|required|is_natural_no_zero'
				),
				array(
					'field' => 'producto',
					'label' => 'producto',
					'rules' => 'trim|required|is_natural_no_zero'
				),
				array(
					'field' => 'cantidad',
					'label' => 'cantidad',
					'rules' => 'trim|required|is_natural_no_zero'
				),
			);

			//cargue de las reglas de validación
			$this->form_validation->set_rules($configDataPost);

			if ($this->form_validation->run() == FALSE)
			{
				$respuesta = array(
					'err' => TRUE,
					'tipo' => 'warning',
					'titulo' => 'Atención!',
					'mensaje' => str_replace("\n", "", validation_errors())
				);

			}
			else
			{

				$datos['idcliente'] = strtolower($this->input->post('cliente'));
				$datos['idproducto'] = strtolower($this->input->post('producto'));
				$datos['cantidad'] = strtolower($this->input->post('cantidad'));
				$datos['fecha'] = date('Y-m-d H:i:s');
				$datos['dia'] = date('Y-m-d');

				$actualizar = $this->pedidos_model->update($datos, $this->input->post('idpedido'));

				if($actualizar['err'])
				{
					$respuesta = array(
						'err' => TRUE,
						'tipo' => 'warning',
						'titulo' => 'Ups!',
						'mensaje' => $actualizar['mensaje']
					);
				}

				else
				{
					$respuesta = array(
						'err' => FALSE,
						'tipo' => 'success',
						'titulo' => 'Super!',
						'mensaje' => 'Su registro se ha actualizado correctamente.'
					);
				}
			}
			echo json_encode($respuesta);
			return;
  }

	public function eliminar(){

		if (!$this->input->is_ajax_request())
			exit('No direct script access allowed');

			$configDataPost = array(

				array(
					'field' => 'idpedido',
					'label' => 'idpedido',
					'rules' => 'trim|required|is_natural_no_zero'
				)
			);

			//cargue de las reglas de validación
			$this->form_validation->set_rules($configDataPost);

			if ($this->form_validation->run() == FALSE)
			{
				$respuesta = array(
					'err' => TRUE,
					'tipo' => 'warning',
					'titulo' => 'Atención!',
					'mensaje' => str_replace("\n", "", validation_errors())
				);

			}
			else
			{

				$elimiar = $this->pedidos_model->delete($this->input->post('idpedido'));

				if($elimiar['err'])
				{
					$respuesta = array(
						'err' => TRUE,
						'tipo' => 'warning',
						'titulo' => 'Ups!',
						'mensaje' => $elimiar['mensaje']
					);
				}

				else
				{
					$respuesta = array(
						'err' => FALSE,
						'tipo' => 'success',
						'titulo' => 'Super!',
						'mensaje' => 'Su registro se ha eliminado correctamente.'
					);
				}
			}
			echo json_encode($respuesta);
			return;

	}

}
