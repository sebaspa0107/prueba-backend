<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">Administrador Pedidos V1.0</div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="{base_url}" target="_blank">Pedidos</a>.</strong> Todos los derechos reservados.
</footer>
