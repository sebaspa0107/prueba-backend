<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Pedidos <small>Pagina para hacer pedidos.</small></h1>
        <ol class="breadcrumb">
            <li><a href="{base_url}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Pedidos</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Hacer un pedido</h3>
            </div>
            <form id="form-hacer-pedido">
              <div class="box-body">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="cliente">Cliente</label>
                    <select name="cliente" id="cliente" class="form-control" required="required">
                      <option value="">Seleccione</option>
                      {clientes}
                      <option value="{id}">{nombre} {apellido}</option>
                      {/clientes}
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="producto">Producto</label>
                    <select name="producto" id="producto" class="form-control" required="required">
                      <option value="">Seleccione</option>
                      {productos}
                      <option value="{id}">{nombre}</option>
                      {/productos}
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="cliente">Cantidad</label>
                    <input type="text" name="cantidad" id="cantidad" class="form-control" required="required" title="Ingresa un valor numérico de máximo 3 caracteres." pattern="[0-9]{1,3}">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <button class="btn btn-primary">Hacer pedido</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
