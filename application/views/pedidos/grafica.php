<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Gráfica <small>pedidos por día.</small></h1>
        <ol class="breadcrumb">
            <li><a href="{base_url}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Gráfica</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Gráfica por día.</h3>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="grafica-dia" style="height: 300px;"></div>
            </div><!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
