<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Pedidos <small>Pagina para ver todos los pedidos.</small></h1>
        <ol class="breadcrumb">
            <li><a href="{base_url}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Ver Pedidos</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Listado de pedidos</h3>
            </div>

            <table id="data-peidos" class="table table-bordered table-striped data-table">
              <thead>
                <tr>
                  <th>Cliente</th>
                  <th>Producto</th>
                  <th>Cantidad</th>
                  <th>Fecha</th>
                  <th width="50">Editar</th>
                  <th width="50">Eliminar</th>
                </tr>
              </thead>
              <tbody>
                {pedidos}
                <tr>
                  <td>{nomcliente} {apecliente}</td>
                  <td>{nomproducto}</td>
                  <td>{cantidad}</td>
                  <td>{fecha}</td>
                  <td><a href="<?php echo base_url(); ?>pedidos/editar/{id}" class="btn btn-warning">Editar</a></td>
                  <td><a href="#" class="btn btn-danger btn-eliminar" data-idpedido="{id}">Eliminar</a></td>
                </tr>
                {/pedidos}
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
