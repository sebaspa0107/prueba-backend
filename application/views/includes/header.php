<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>{title}</title>
    <link rel="shortcut icon" href="{base_url}images/favicon.png">
    <meta property="og:title" content="{title}" />
    <meta property="og:type" content="{type}" />
    <meta property="og:url" content="{base_url}" />
    <meta property="og:image" content="{imageFb}" />
    <meta name="viewport" content="width=device-width, maximum-scale=1, user-scalable=no">

    <!-- estilos -->
    <link rel="stylesheet" href="{base_url}css/{estilo}.css">
    <link rel="stylesheet" href="{base_url}css/morris.css">
    <script type="text/javascript">
      var base_url = '{base_url}';
      var pagina = '{pagina}';
    </script>
  </head>
  <body id="{pagina}" class="{class_body}">
