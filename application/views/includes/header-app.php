<header>
	<div class="line-black">
		<div class="brand">
			<div class="item-header"></div>
			<div class="item-header"><a class="logo" href="{base_url}"><img src="{base_url}images/logo-saimus.png" alt=""></a></div>
			<div class="item-header"></div>
			<div class="item-header"></div>
			<div class="item-header"><a class="logo" href="{base_url}"><img src="{base_url}images/chimp-saimus.png" alt=""></a>
			</div>
		</div>

		<div class="navigation">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
						<!-- <a class="navbar-brand" href="#"></a> -->
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li class="<?= ($pagina == 'armatubici') ? 'active' : '' ?>"><a href="{base_url}arma-tu-bici">Arma tu bici <span class="sr-only">(current)</span></a></li>
							<!-- <li class="<?//= ($pagina == 'experiencia') ? 'active' : '' ?>"><a href="{base_url}">Experiencia</a></li> -->
							<!-- <li class="<?//= ($pagina == 'accesorios') ? 'active' : '' ?>"><a href="{base_url}accesorios">Accesorios</a></li> -->
							<!-- <li class="<?//= ($pagina == 'equiposaimus') ? 'active' : '' ?>"><a href="{base_url}equipo-saimus">Equipo Saimus</a></li> -->
						</ul>
					</div>
					<!-- /.navbar-collapse -->
				</div>
				<!-- /.container-fluid -->
			</nav>
		</div>
</header>
